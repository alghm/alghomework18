﻿var graphArray = new int[,]
{
    { 0, 1, 0, 0, 0 },
    { 1, 0, 1, 1, 0 },
    { 0, 1, 0, 1, 1 },
    { 0, 1, 1, 0, 1 },
    { 0, 0, 1, 1, 0 }
};

var graph = new Graph(graphArray);
var shortestPaths = graph.DijkstraAlg(2);

foreach (var (edge, dist) in shortestPaths)
{
    Console.WriteLine($"Min distance for {edge.v1} - {edge.v2}: {dist}");
}


Console.WriteLine();

public class Edge
{
    public int v1 { get; set; }
    public int v2 { get; set; }

    public Edge(int v1, int v2)
    {
        this.v1 = v1;
        this.v2 = v2;
    }
}

public class Graph
{
    private int[,] graph;

    public Graph(int[,] graph)
    {
        this.graph = graph;
    }

    public IEnumerable<KeyValuePair<Edge, int>> DijkstraAlg(int src)
    {
        int V = graph.GetLength(0);
        int[] dist = new int[V];
        bool[] set = new bool[V];

        for (int i = 0; i < V; i++)
        {
            dist[i] = int.MaxValue;
            set[i] = false;
        }

        dist[src] = 0;

        for (int count = 0; count < V - 1; count++)
        {
            int u = MinDistance(dist, set);
            set[u] = true;

            for (int v = 0; v < V; v++)
            {
                if (!set[v] && graph[u, v] != 0 && dist[u] != int.MaxValue && dist[u] + graph[u, v] < dist[v])
                {
                    dist[v] = dist[u] + graph[u, v];
                }
            }
        }

        var shortestPaths = new List<KeyValuePair<Edge, int>>();

        for (int i = 0; i < V; i++)
        {
            if (dist[i] != int.MaxValue)
            {
                shortestPaths.Add(new KeyValuePair<Edge, int>(new Edge(src, i), dist[i]));
            }
        }

        return shortestPaths;
    }

    private int MinDistance(int[] dist, bool[] set)
    {
        int min = int.MaxValue, min_index = -1;

        for (int v = 0; v < dist.Length; v++)
        {
            if (set[v] == false && dist[v] <= min)
            {
                min = dist[v];
                min_index = v;
            }
        }

        return min_index;
    }
}
